# Assume venv; for dev only

.PHONY: init test style sandbox dependencies

PROJECT_NAME := $(shell basename "$(CURDIR)")

all: style test

# Set up a new venv
init:
	rm -rf .venv
	python3.11 -m venv .venv
	.venv/bin/pip install --upgrade pip poetry

# Install dependencies
dependencies:
	.venv/bin/poetry install --no-root

# Run tests and report coverage
test:
	.venv/bin/python -m coverage run --source=$(PROJECT_NAME) -m unittest -v
	.venv/bin/python -m coverage report -m --fail-under 95

# Check code style - use AutoPEP8 to clean up some
style:
	autopep8 -ir .
	.venv/bin/pycodestyle $(PROJECT_NAME)
	.venv/bin/pycodestyle test

build:
	rm -rf dist
	.venv/bin/poetry build

# Reset the sandbox for casual testing
sandbox:
	rm -rf ./sandbox/test-target/*
	rm -rf ./sandbox/test-source/*
	cp -r ./sandbox/test-origin/* ./sandbox/test-source